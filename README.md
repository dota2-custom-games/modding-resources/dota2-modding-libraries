# Dota 2 Modding Libraries

This repository contains an aggregated set of libraries
for the Dota 2 arcade. These libraries can be used to help in the process
of making custom games inside the Dota engine.

The libraries linked here are the combined work of Almouse and Shooper -
most were created or adapted from work inside the SWAT: Reborn gamemode.
However, much of the work is built on top of contributions to the modding
scene from giants such as BMD and Noya.

## Using Libraries

In most cases, the code presented here is not set up in the traditional
Dota 2 folder structure. This is mostly for ease-of-reading, avoiding
having to drudge through multiple subfolders to examine each file. As a
result, importing the code may require some additional steps. However,
the general process is:

- Download the contents of one of the subfolders of the repository (e.g.
the `MinimapManager` folder)
- Move the files from the library into either the `game` or `content` folders
of your existing Dota arcade project (depending on the specific filetype)
- Add include statements to your existing source files (e.g. `require` statements
in Lua, or `CustomUIElement` tags in panorama)
- Adjust any include statements inside the library code files depending on the
specific folder structure of your mod
- Do any other necessary pre-work (e.g. `precache` statements in your `addon_game_mode.lua`,
or addition of new nettables)
- Make use of the functionality!

## List of Libraries

The list of libraries follows. It is presented in alphabetical order.

***

- [`BottomPanel`](BottomPanel/) is an example replacement for the core Dota
bottom panel, containing the portrait box, attribute box and the ability
tray.

[![](BottomPanel/banner.png)](BottomPanel/)

***

- [`MinimapManager`](MinimapManager/) adds additional functionality to the Dota minimap such as panorama-driven icons and region highlighting and map resizing.

***

- [`ProgressBars`](ProgressBars/) allows for the addition of progress bars that
follow units, similar to the status bars of the Stunned or Taunted modifiers
in core Dota

[![](ProgressBars/banner.png)](ProgressBars/)

***

## Other Libraries

I recommend several other libraries, maintained by users on separate Git repositories:

- [BuildingHelper](https://github.com/MNoya/BuildingHelper) by Noya gives support
for WC3-style buildings and build abilities



## Adding Libraries

To add more libraries to this repository, the following steps should be taken:

- Create a new subfolder of the main repo for the library
- Create a `card.png` which includes the library's name. This will be used on the library collection website. It should be 203x114 pixels in size (this is a magic number for the website)
- Create a `banner.png`, containing some hint to the library's contributions. This will be used on this page. It should be 900x150 pixels in size (this is to fill the text width in GitLab's css)
- Write a README.md for the library. It should include a description of the functionality and basic setup instructions
- Add the code files in one or more subfolders of the core library folder. Use whatever file structure you like, but try to keep it simple so that a user can easily skim through the code
- Ensure that the library itself contains documentation **inside at least one of the code files** for usage or important API calls. This is important for helping the distributability of the code!

I am willing to help with some of the steps if needed, please get in touch (`Almouse#5461` on Discord) 