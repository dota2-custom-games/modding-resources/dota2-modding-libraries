# Progress Bars

![](banner.png)

v1.0.0, by Almouse

The Progress Bars library provides a Panorama-based approach for
adding unit-tracking "Progress Bars" to Dota 2 arcade mods. Progress Bars
display either the stacks or duration of a modifier, allowing emulation
of Dota mechanics like the status bars above units.

## Setup

Take the following steps to set up the library:

- Copy `ProgressBars.lua` to your `game/scripts/vscripts` folder.
- Inside one of your core script files (such as `addon_game_mode.lua`), add
    `require(ProgressBars)`, changing the path to the lua file if needed.
- Modify `scripts/custom_net_tables.txt` to include the `progress_bars` nettable.
- Copy `progress_bars.xml`, `progress_bars.css` and `progress_bars.js` to your
    `content/panorama` folder (to the `layout`, `styles` and `scripts` folders, respectively)
- Modify the marked paths in `progress_bars.xml` to match the correct locations
of the CSS and JS files.
- Inside `content/panorama/layout/custom_game/custom_ui_manifest.xml`, add
`<CustomUIElement type="Hud" layoutfile="file://{resources}/layout/custom_game/progress_bars.xml" />`, changing the path to the XML file if necessary.

## Usage

Detailed usage instructions can be found inside the `ProgressBars.lua` file.
The general workflow is:

- Use the Lua calls `ProgressBars:AddProgressBar(unit, modifierName, configuration)`
to add a Progress Bar to a unit
    - `unit` is a handle to a unit (not an entityID)
    - `modifierName` is a string
    - `configuration` is a table with fields:
        - `progressBarType`: "duration" or "stacks"
            - "duration": the width of the progress bar will be `currentDuration/maxDuration` (read from the modifier properties)
            - "stacks": the width of the progress bar will be `currentStacks/maxStacks` (read from the parameters)
        - `reversedProgress`: bool. Progress will be 1-progress instead of progress. Good for counting up instead of down
        - `maxStacks`: Number used for stack-based modifiers to mark the maximum
        possible stack count
        - `style`: String style applied to the parent (children can access through inheritance)
        - `text`: String label applied to the progress bar: this text is not localised by default
        - `textSuffix`: String suffix that will be added to the end of a progress bar: this text will be localised (so can be used for units of measure)
- For example:
    ```lua
    local config = {
            progressBarType = "stacks",
            reversedProgress = false,
            style = "EnrageStacks",
            maxStacks = 100
    }
    ProgressBars:AddProgressBar(caster, "modifier_super_mutant_rage_stacks", config)
    ```
- Use the Lua call `ProgressBar:RemoveProgressBar(unit, modifierName)` to remove
a Progress Bar (they should also automatically be cleaned up)
- Be careful if you are attaching a Progress Bar as part of a modifier to only
run this code serverside
- You can design new stylesheets for Progress Bars, or view existing ones inside
`progress_bars.css`


## Contributions

The following contributions would help improve this library:

- Allow for an approach that uses particle-based progress bars, such
as those used by core Dota. Particle-based solutions are generally more
efficient, but might be harder to customise
- Add more example CSS setups to be used in `progress_bars.css`

## Thanks

The code for positioning panels is derived from that of BMD.

Thanks to efe for their help testing the library.