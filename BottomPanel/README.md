# Bottom Panel

![](banner.png)

v0.1.0, by Almouse

This is a re-implementation of the core Dota bottom panel. There is a focus on maintaining efficient operation
(as opposed to the sample implementation provided by Valve, which makes excessive use of `BLoadLayout` calls).

The following core HUD features are included:

- Health and Mana Bars
- Portrait and Multiunit Selection Boxes
- Experience and Unit name boxes
- Attribute trays
- Ability bar
- Buff/Debuff bars

The following core HUD features are **not** included:

- The inventory and stash boxes (try the Containers library)
- The talent tree
- Some details on the ability tray

This library is currently considered incomplete - some of the features may be unstable and it requires
more work, but it may be a useful template. Please consider contributing.

## Setup

Take the following steps to set up the library:

- Copy the files in the `content` folder to your `content/panorama` folder, moving files into either the
    `layout`, `style`, `scripts` or `image` subfolders, depending on their type.
- Modify the marked paths in `bottom_panel.xml` and `ability_tray.xml` to match the correct locations
of the CSS and JS files.
- Inside `content/panorama/layout/custom_game/custom_ui_manifest.xml`, add
`<CustomUIElement type="Hud" layoutfile="file://{resources}/layout/custom_game/bottom_panel.xml" />`, changing the path to the XML file if necessary.
- Inside `content/panorama/layout/custom_game/custom_ui_manifest.xml`, add
`GameUI.SetDefaultUIEnabled( DotaDefaultUIElement_t.DOTA_DEFAULT_UI_ACTION_PANEL, false );` inside `<script>` tags (this disables the
    default bottom panel).

## Usage

This library is plug-and-play, and should work immediately after adding.
You may want to add or modify the CSS, or the ways some of the functions work
by editing the code.

## Contributions

The following contributions would help improve this library:

- Improved or alternate styles for the bottom panel
- Addition of particles on the ability tray, as in core Dota
- Addition of missing remaining elements from the core Hud
- Support for gold or resource costs for abilities (e.g. for integrating with BuildingHelper)
- Addition of default tooltips for the hoverable elements
- Support for alternate attribute systems, alternate resource systems (health/mana) etc.

## Thanks

A lot of the XML layout data is based on information from the Valve source files.