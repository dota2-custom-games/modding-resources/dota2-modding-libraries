# Minimap Manager

![](banner.png)

v1.0.0, by Almouse

The Minimap Manager Library adds additional functionality to the core Dota minimap. This includes the ability to define custom minimap icons using panorama image files (rather than Dota KV values), as well as highlighting regions. You can also resize which parts of the map are shown on the minimap (for example, for travelling between regions).

## Setup

Take the following steps to set up the library:

- Copy `MinimapManager.lua` to your `game/scripts/vscripts` folder.
- Inside one of your core script files (such as `addon_game_mode.lua`), add
    `require(MinimapManager)`, changing the path to the lua file if needed.
- Modify `scripts/custom_net_tables.txt` to include the `minimap_settings`, `minimap_objects`, `minimap_regions` and `minimap_heroes` nettables.
- Copy `objective_minimap.xml`, `objective_minimap.css` and `objective_minimap.js` to your
    `content/panorama` folder (to the `layout`, `styles` and `scripts` folders, respectively)
- Modify the marked paths in `objective_minimap.xml` to match the correct locations
of the CSS and JS files.
- Inside `content/panorama/layout/custom_game/custom_ui_manifest.xml`, add
`<CustomUIElement type="Hud" layoutfile="file://{resources}/layout/custom_game/objective_minimap.xml" />`, changing the path to the XML file if necessary.

## Usage

Detailed usage instructions can be found inside the `MinimapManager.lua` file.

## Contributions

The following contributions would help improve this library:

- Allow for considerations about different teams: right now, all players are assumed to be on the same team.
- Improved rendering of minimap pings and other events: these resize in an awkward manner when the minimap is resized. Unfortunately it seems tricky to do this in a way that behaves with custom hero icons.
