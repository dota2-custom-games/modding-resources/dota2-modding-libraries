"use strict";

/*
    Minimap Panoramanager 
    Version 1.0.0
    by Almouse

    Panorama side of the MinimapManager and ObjectiveManager lua files
    Creates a minimap overlay with interactive icons and regions
    Also supports custom hero icons

    Some parameters must be set below (CONSTANT_TERMS)  

    Objective panels and minimap icons have tooltips that should be
    localised (see below)

*/

// How often should hero icons (tracked locally) be updated?
var HERO_ICON_UPDATE_INTERVAL = 0.1;
// How often should "regular" icons be updated?
// This is just for re-synchronisation in case a nettable update is skipped
// (e.g. from panorama lag)
var REGULAR_ICON_UPDATE_INTERVAL = 10;

// Is objective manager active? If not, we should hide it!
var OBJECTIVE_MANAGER_ACTIVE = false

/* Prefix for tooltips
    <PREFIX>_tooltip_objective_completion 
        Constants for how "complete" an objective is
        (e.g. "incomplete", "complete", "perfect")
    <PREFIX>_tooltip_objective_<OBJECTIVE_ID>_<COMPLETION>
        Tooltip for each objective based on how complete it is
        (e.g. what must be done)
    <PREFIX>_tooltip_objective_<OBJECTIVE_ID>_title
        Name of an objective
    <PREFIX>_tooltip_minimap_icon_<ICON_ID>
        Name of an icon
    <PREFIX>_tooltip_minimap_region_<REGION_ID>
            Name of a region 
*/
var TOOLTIP_PREFIX = "PP"

// Icon choices
var ICON_LOOKUP = {
    // e.g.
    //"pow_white": "url('file://{images}/objectives/pow_icon.png')",
    // no-icon
    "no_icon": "none"
};

// ------------------------------------------------------------- //
// -------------------------- Variables ------------------------ //
// ------------------------------------------------------------- //

// References to panels on/in the minimap
var minimapOuterPanel = null;
var minimapContainerPanel = null;
var minimapPanel = null;

// New panels that will be created inside the dota hud
var minimapMaskPanel = null;
var minimapOverlayPanel = null;

// Local panel that will be used for storing tooltips
// New panels are created inside this panel so that they'll load
// the correct CSS file
var dummyPanel = $("#Dummy");

// References to panels in the objective bar
// $ referencing doesn't work because they'll be inside the dota HUD
var objectivePanelToggle = null;
var objectiveRoot = $("#ObjectiveRoot");
var objectiveBackground = null;
var objectiveMainPanel = null;

// Trays for the different objectives
var primaryObjectivePanel = null;
var secondaryObjectivePanel = null;
var tertiaryObjectivePanel = null;

// A lookup of objective name-->{"panel",
        // "text", "completion", "icon", "hoverIcons", "hideIcons", "category"}
// This should correspond to the objectives nettable
var objectives = {};

// A array of panels that are highlighted
// (the actual highlight panel is a child of the panel being highlighted)
var highlightedPanels = [];
var highlightedRegions = [];

// A lookup for which icon styles we want to be able to see
// Used in combination with 'hide' buttons on objectives
var iconVisibility = {};

// json of entityID->{"panel": iconpanel, "icon": iconName}
// The location of these entities is tracked by the ~lua~ code,
// because panorama can only track entities that the local player can see
// We might want some icons that are visible on the map even if not
// visible to the player (e.g. bosses in Normal)
var minimapIcons = {};

// json of entityID->{"panel": iconPanel}
// used for heroIcons, which are tied to a specific unit,
// and assume we have local knowledge of the entity for updating position
var heroIcons = {};

// json of key->{"style", "panel", "category", "min_x", "max_x", "min_y", "max_y"}
// style refers to css styles
// category is used for tooltip and hovering purposes
var minimapRegions = {};

// World min and max coordinates
// These should in theory be constant depending on the map file
// Assigned by lua call
var world_min_x = -16352;
var world_max_x = 16352;
var world_min_y = -16352;
var world_max_y = 16352;

// Map min and max coordinates (i.e. the minimap)
// These could be variable if we want to change which part of the minimap
// we are looking at
// For example, if the map expands or changes during gameplay
// Assigned by lua call
var map_min_x = -16352;
var map_max_x = 16352;
var map_min_y = -16352;
var map_max_y = 16352;

// Covers shifting the minimap inside its container, in case it's
// not a square (and so doesn't fit the full box)
var map_x_offset = 0;
var map_y_offset = 0;

// This is the size of the box that contains the minimap.
// 244 is the 'default', but it can change depending on
// the ExtraLargeMinimap css property
var minimapContainerSize = 244;

//  Constant array used by objectives
var categoryLookup = {
    "Primary": $("#PrimaryObjectivesBar"),
    "Secondary": $("#SecondaryObjectivesBar"),
    "Tertiary": $("#LandmarksBar")
};

// Because some of our panels are embedded in the DotA UI, we cannot
// always rely on pure CSS to jiggle the styles of things. So, we have
// to track them using javascript
var currentlyFlipped = null;
var currentlyWideMap = null;
var currentAspectRatio = 1; // "falsey"

// ------------------------------------------------------------- //
// ---------------------- Utilities/Helpers -------------------- //
// ------------------------------------------------------------- //

// Try to find a panel, and if it doesn't exist, then create it
// Dota gets angry when we recompile things using CreatePanel, so
// this helps us out a little
// Particularly useful for debug mode if we reload this panel
function CreateOrReferencePanel(panelType, parent, panelId)
{
    var hud = $.GetContextPanel().GetParent();
    for(var i = 0; i < 100; i++) {
        if(hud.id != "Hud") {
            hud = hud.GetParent();
        } else {
            break;
        }
    };

    var testPanel = hud.FindChildTraverse(panelId);
    if (testPanel) return testPanel
    else return $.CreatePanel(panelType, parent, panelId)

};

// indexOf keeps breaking so
function contains(arr, val){
    for (var i = 0; i < arr.length; i++ )
    {
        if (arr[i] == val) {
            return true;
        };
    };
    return false;
};

// Cast a dumb json object from lua into a beautiful array
function jsonToArray(json){
    var output = [];
    $.Each(json, function(key, val){
        output.push(key); // Yeah I don't understand why this isn't val either
    })
    return output
}

// Check if a received nettable is empty. Because... I don't know why
function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}

// Check is a unit is invisible
function IsUnitInvisible(unitIndex)
{
    for (var i = 0; i < Entities.GetNumBuffs(unitIndex); i++)
    {
        var buffName = Buffs.GetName(unitIndex, Entities.GetBuff(unitIndex, i))

        if (buffName == "modifier_invisible")
        {
            return true
        }
    }

    return false
}

// This is run once at the start of execution
// Forcefully updates our local data to match the nettables
function SynchroniseNetTables(){

    var objectiveTable = CustomNetTables.GetAllTableValues("objectives");

    //$.Msg("nameTable: "+nameTable)

    for (var index of objectiveTable) {
        OnObjectiveTableChange( objectiveTable, index["key"], index["value"] )
    }

    var minimapTable = CustomNetTables.GetAllTableValues("minimap_objects");

    for (var index of minimapTable) {
        OnMinimapTableChange( minimapTable, index["key"], index["value"] )
    }

    var regionTable = CustomNetTables.GetAllTableValues("minimap_regions");

    for (var index of regionTable) {
        OnRegionTableChange( regionTable, index["key"], index["value"]);
    }

    var heroTable = CustomNetTables.GetAllTableValues("minimap_heroes");
    for (var index of heroTable) {
        OnHeroTableChange( heroTable, index["key"], index["value"]);
    }

}

// ------------------------------------------------------------- //
// ------------------------ Objective Tray --------------------- //
// ------------------------------------------------------------- //
/*
    This section synchronises with ObjectiveManager
*/

// Update local state to match nettable
function OnObjectiveTableChange(table_name, key, data){

    if (data == null || isEmpty(data) ) { // Or some other falsy thing?
        AttemptToRemoveObjective(key)
        return false
    }

    if (objectives[key] == null) {
        objectives[key] = []
        objectives[key]["category"] = data["category"];
        objectives[key]["panel"] = CreateObjectivePanel(categoryLookup[data["category"]])
        $.Msg("Creating new objective for "+key)
    };

    if (objectives[key]["panel"]) {

        objectives[key]["icon"] = data["icon"];
        objectives[key]["text"] = data["text"];
        objectives[key]["completion"] = data["completion"];

        //$.Msg("Minimap Panoramanager | Updating objective with icon: "+data["icon"])
        //$.Msg("Minimap Panoramanager | Updating objective with completion: "+data["completion"])
        //$.Msg("Minimap Panoramanager | Updating objective with category: "+data["category"])

        objectives[key]["hoverIcons"] = jsonToArray(data["hoverIcons"]);
        objectives[key]["hideIcons"] = jsonToArray(data["hideIcons"]);

        UpdateObjectivePanel(key);
    };

};


function AttemptToRemoveObjective(key){
    $.Msg("Minimap Panoramananger | Deleting objective "+str(key))
    var containerPanel = objectives[key]["panel"];
    containerPanel.DeleteAsync(0);
    objectives[key] = null;
};

// Creates an objective panel.
// parentPanel: the parent of the panel (primaryObjectivePanel, secondaryObjectivePanel, tertiaryObjectivePanel)
function CreateObjectivePanel(parentPanel){
    $.Msg("Panoramanager | Creating Objective Panel");


    // Count the number of children of the parent
    var numChildren = parentPanel.Children().length;

    if (numChildren >= 4) {
        $.Msg("Minimap Panoramanager | Tried to overfill an objective tray")
        return false
    }

    var objContainer = $.CreatePanel( "Panel", dummyPanel, "" );
    objContainer.BLoadLayoutSnippet( "ObjectivePanel" );

    // Set the correct parent
    objContainer.SetParent(parentPanel);

    return objContainer
};

// Called on clicking the thing to expand the minimap panel
function ToggleObjectivePanel(){
    objectiveRoot.ToggleClass("Expanded");
};

// Traditionally called after the nettable updates
function UpdateObjectivePanel(objective){

    //$.Msg("Minimap Panoramanager | UpdateObjectivePanel on "+objective);

    var objContainer = objectives[objective]["panel"];

    if (objContainer == null) {
        $.Msg("Tried to update an invalid objective!");
        return false
    }

    var objIcon = objContainer.FindChildrenWithClassTraverse("Objective")[0];
    var objCancel = objContainer.FindChildrenWithClassTraverse("HideButton")[0];
    var objText = objContainer.FindChildrenWithClassTraverse("ObjectiveText")[0];

    var icon = objectives[objective]["icon"];
    var text = objectives[objective]["text"];
    var completion = objectives[objective]["completion"];
    var hoverIcons = objectives[objective]["hoverIcons"];
    var hideIcons = objectives[objective]["hideIcons"];
    var category = objectives[objective]["category"];

    if (categoryLookup[category] != objContainer.GetParent()) {
        objContainer.SetParent(categoryLookup[category])
    }

    for (var completionType of ["Incomplete", "Complete", "Perfect", "Inactive"]) {
        if (completion == completionType){
            objIcon.SetHasClass(completionType, true);
        } else {
            objIcon.SetHasClass(completionType, false);
        }
    };

    // Add the necessary classes
    // For some reason, it's important that we force-set these options
    // As it forces a re-render of the background
    // Without it, often the background is warped
    objIcon.style.backgroundImage = ICON_LOOKUP[icon];
    objIcon.style.backgroundRepeat = "no-repeat";
    if (text.length > 0){
        objIcon.style.backgroundSize = "50% 50%";
        objIcon.style.backgroundPosition = "50% 40%";
    } else {
        objIcon.style.backgroundSize = "50% 50%";
        objIcon.style.backgroundPosition = "50% 45%";
    };

    if (text != null && text.length > 0) {
        objText.visible = true;
        objText.text = text;
        objText.SetHasClass("SmallFont", (text.length > 5))
    } else {
        objText.visible = false;
    }

    // --- Tooltips --- //
    objContainer.SetPanelEvent("onmouseover", function(){
        HighlightAll(hoverIcons);

        // Objective Name
        var tooltipText = "<font color='#8585B6'>" + $.Localize(TOOLTIP_PREFIX+"_tooltip_objective_"+objective+"_title") + "</font>";
        tooltipText += "<br />"

        // Completion and current text
        var secondLine = $.Localize(TOOLTIP_PREFIX+"_tooltip_objective_"+objectives[objective]["completion"]);
        var objectiveText = objectives[objective]["text"]
        if (objectiveText != null &&
            objectiveText != "" &&
            (objectives[objective]["completion"]!="Inactive"))
        {
            secondLine += " ("
            secondLine += objectiveText
            secondLine += ")"
        }
        if (secondLine != "") {
            tooltipText += secondLine;
            tooltipText += "<br />";
        }

        // Information tooltip
        tooltipText += $.Localize(TOOLTIP_PREFIX+"_tooltip_objective_"+
                        objective+
                        "_"+
                        objectives[objective]["completion"]);

        $.DispatchEvent( "DOTAShowTextTooltip", $("#Dummy"),
            tooltipText
            );

    });
    objContainer.SetPanelEvent("onmouseout", function(){
        RemoveAllHighlights()
        $.DispatchEvent("DOTAHideTextTooltip", $("#Dummy"))
        });
    // --- Tooltips --- //

    // --- Hide icons for an objective --- //
    if (hideIcons!=""){ // Some icons cannot be hidden!
        objCancel.RemoveClass("Hidden");
        objCancel.SetPanelEvent("onactivate", function(){
            HideIcons(hideIcons);
            objCancel.ToggleClass("Activated");
        });
    } else {
        objCancel.AddClass("Hidden");
    };

    var parentPanel = objContainer.GetParent()

};


// ------------------------------------------------------------- //
// ------------------- Minimap Boundary Changes ---------------- //
// ------------------------------------------------------------- //

function OnSettingTableChange(table_name, key, data){

    if (key == "minimap_boundaries" ) {
        OnSetMinimap(data);
    }

    if (key == "world_boundaries" ) {
        OnSetWorld(data);
    }

}


// Arbitrary setter function for convenience
function SetMinimapBoundaries(min_x,max_x,min_y,max_y){
    map_min_x = min_x;
    map_max_x = max_x;
    map_min_y = min_y;
    map_max_y = max_y;
};

function OnSetMinimap(keys){
    $.Msg("Minimap Panoramanager | New minimap boundaries: x("+
            keys.min_x+", "+
            keys.max_x+"), y("+
            keys.min_y+", "+
            keys.max_y+")"
    );

    SetMinimapBoundaries(keys.min_x,keys.max_x,keys.min_y,keys.max_y) ;

    UpdateMinimapBoundaries();

    // We have a race condition with the lua code, which will change
    // the boundaries of the map at some point. These scheduled functions
    // are to ensure that the panorama (should) eventually catch up
    UpdateAllIcons();

};

function SetWorldBoundaries(min_x,max_x,min_y,max_y){
    world_min_x = min_x;
    world_max_x = max_x;
    world_min_y = min_y;
    world_max_y = max_y;

};

function OnSetWorld(keys){
    $.Msg("Minimap Panoramanager | New world boundaries: x("+
            keys.min_x+", "+
            keys.max_x+"), y("+
            keys.min_y+", "+
            keys.max_y+")"
    )

    SetWorldBoundaries(keys.min_x,keys.max_x,keys.min_y,keys.max_y) ;

    UpdateMinimapBoundaries();

    UpdateAllIcons();
}

function OnHeroTableChange(table_name, key, data)
{
    var intKey = Number(key) // Lua keys must be of type string, coerce it here

    if (data == null || isEmpty(data) ) // Probably means we should remove the icon
    {
        RemoveHeroIcon(intKey);
        return true
    }

    if (!heroIcons[intKey]) { // Create new hero panel
        $.Msg("Adding new hero icon for key: "+key)
        heroIcons[intKey] = {}

        var heroPanel = CreateOrReferencePanel("Panel", dummyPanel, intKey);
        heroPanel.BLoadLayoutSnippet( "HeroMinimapIcon" );
        heroPanel.hittest = false;

        heroPanel.SetParent(minimapOverlayPanel);

        heroIcons[intKey]["panel"] = heroPanel;

        var iconPanel = heroPanel.FindChildrenWithClassTraverse("PortraitIcon")[0];
        
        iconPanel.style.backgroundImage = ICON_LOOKUP[data["minimapIcon"]] || "none";

        var local = Players.GetLocalPlayer() ;
        var owner = Entities.GetPlayerOwnerID( intKey );
        var isLocal = (local == owner);

        $.Msg("New hero unit. Entity ID: "+intKey+", local player: "+local+", owner: "+owner);

        heroPanel.AddClass("PlayerColor"+data["playerIndex"]);
        heroPanel.SetHasClass("HeroIconPrimaryA", data["isPrimary"] && isLocal);
        heroPanel.SetHasClass("HeroIconPrimaryB", data["isPrimary"] && isLocal);
        heroPanel.AddClass(data["class"]);

    }

    UpdateHeroIcon(intKey);

}

function OnRegionTableChange(table_name, key, data){

    if (data == null || isEmpty(data) ) {
        $.Msg("Minimap Panoramanager | Removing minimap region")
        AttemptToRemoveRegion(key)
    } else {

        if (!minimapRegions[key]) { // We need to create a new panel!
            //$.Msg("Making a new minimap icon!");
            minimapRegions[key] = {};
            var regionPanel = CreateOrReferencePanel("Panel", dummyPanel, key);
            regionPanel.AddClass("MinimapRegion");
            //iconPanel.hittest = false;
            regionPanel.AddClass(data["style"]);
            regionPanel.SetParent(minimapOverlayPanel);
            minimapRegions[key]["panel"] = regionPanel;
        };

        minimapRegions[key]["style"] = data["style"];
        minimapRegions[key]["category"] = data["category"];

        minimapRegions[key]["min_x"] = data["min_x"];
        minimapRegions[key]["max_x"] = data["max_x"];
        minimapRegions[key]["min_y"] = data["min_y"];
        minimapRegions[key]["max_y"] = data["max_y"];

        // Actually position the icon
        UpdateRegion(key)
    }

}

// ------------------------------------------------------------- //
// -------------- Minimap Placement Utilities ------------------ //
// ------------------------------------------------------------- //

function PositionPanelAtMapCoordinates(panel, x, y){
    var coords = WorldToMinimapXY(x,y);

    panel.style.transform = "translate3d("+(coords[0]-panel.actuallayoutwidth/panel.actualuiscale_x/2)+
        "px,"+(-coords[1]+panel.actuallayoutheight/panel.actualuiscale_y/2)+"px,0)"

}


// Check that coordinates fall within the minimap
function ValidCoords(xposition, yposition){
    return (xposition > map_min_x &&
        xposition < map_max_x &&
        yposition > map_min_y &&
        yposition < map_max_y)
};


// Takes an x,y in world coordinates and returns an x,y in map coordinates
function WorldToMinimapXY(world_x, world_y){
    //$.Msg("Running WorldToMinimapXY");

    //minimapContainerSize = minimapContainerPanel.actuallayoutwidth/minimapContainerPanel.actualuiscale_x;

    var x_width = (map_max_x-map_min_x);
    var y_width = (map_max_y-map_min_y);

    // proportions are from 0-1 if the unit is on the map
    // this accounts for e.g. the map not having the same centre as the world
    var x_proportion = (world_x-map_min_x)/x_width;
    var y_proportion = (world_y-map_min_y)/y_width;

    // The extra twiddling here is for if the map is not square
    if (x_width < y_width) {
        return [ (x_proportion*x_width/y_width+0.5*(1-x_width/y_width))*minimapContainerSize,
                 y_proportion*minimapContainerSize ]
    } else {
        return [ x_proportion*minimapContainerSize,
                 (y_proportion*y_width/x_width+0.5*(1-y_width/x_width))*minimapContainerSize ]
    }
};

// Rescale the minimap inside the container, adding a mask if needed
function UpdateMinimapBoundaries(){

    $.Msg("Running UpdateMinimapBoundaries");

    // Scale to the largest difference in coordinates
    var x_diff = map_max_x - map_min_x;
    var y_diff = map_max_y - map_min_y;

    if (Game.IsHUDFlipped()) {
        var shouldFlipX = "scaleX(-1)";
    } else {
        var shouldFlipX = "scaleX(1)";
    }

    minimapOverlayPanel.style.transform = shouldFlipX;

    if (x_diff > y_diff) { // Branching for placing an offset

        // Rescale factor is the ratio between the world max size
        // and the intended map size
        // Assume the world is rectangular (problematic?)
        var rescaleFactor = (world_max_x - world_min_x) / x_diff
        var excessSpace = x_diff-y_diff;

        // "Push" offsets are the difference between the intended offsets
        var x_offset = (map_min_x - world_min_x)/(world_max_x-world_min_x)*
                minimapContainerSize*rescaleFactor;
        var y_offset = (map_max_y - world_max_y-excessSpace/2)/
                (world_max_y-world_min_y)*minimapContainerSize*rescaleFactor;

        minimapPanel.style.width = String(minimapContainerSize * rescaleFactor)+"px";
        minimapPanel.style.height = String(minimapContainerSize * rescaleFactor)+"px";
        minimapPanel.style.transform = "translate3d("+(-x_offset)+
                    "px, "+y_offset+"px, 0px) "+shouldFlipX;


        // Honestly I can't remember what this all does
        // I believe this is to give a black background around the minimap,
        // Rather than whatever the default background is
        //minimapMaskPanel.style.height = String(minimapContainerSize* x_diff/y_diff)+"px";
        //minimapMaskPanel.style.width = String(minimapContainerSize+50)+"px";
        var effectiveMaskHeight = (minimapContainerSize*(1-y_diff/x_diff))/2;
        minimapMaskPanel.style.borderLeft = "0px solid black";
        minimapMaskPanel.style.borderRight = "0px solid black";
        minimapMaskPanel.style.borderTop = ""+effectiveMaskHeight+"px solid black";
        minimapMaskPanel.style.borderBottom = ""+effectiveMaskHeight+"px solid black";

    } else { // Same but with y instead of x, we could refactor this

        // Rescale factor is the ratio between the world max size
        // and the intended map size
        // Assume the world is rectangular (problematic?)
        var rescaleFactor = (world_max_y - world_min_y) / y_diff
        var excessSpace = y_diff-x_diff;

        // "Push" offsets are the difference between the intended offsets
        var x_offset = (map_min_x - world_min_x-excessSpace/2)/(world_max_x-world_min_x)*
                minimapContainerSize*rescaleFactor;
        var y_offset = (map_max_y - world_max_y)/
                (world_max_y-world_min_y)*minimapContainerSize*rescaleFactor;

        minimapPanel.style.width = String(minimapContainerSize * rescaleFactor)+"px";
        minimapPanel.style.height = String(minimapContainerSize * rescaleFactor)+"px";
        minimapPanel.style.transform = "translate3d("+(-x_offset)+
                    "px, "+y_offset+"px, 0px)"+shouldFlipX;

        // We have to set the height such that it matches with the actual height of the
        // minimap. But because we have css margins, we want to do it using css-style values
        // But dota is an ass, so here's a workaround

        //minimapMaskPanel.style.width = String(minimapContainerSize* x_diff/y_diff)+"px";
        //minimapMaskPanel.style.height = String(minimapContainerSize+50)+"px";
        var effectiveMaskWidth = (minimapContainerSize*(1-x_diff/y_diff))/2;
        minimapMaskPanel.style.borderLeft = ""+effectiveMaskWidth+"px solid black";
        minimapMaskPanel.style.borderRight = ""+effectiveMaskWidth+"px solid black";
        minimapMaskPanel.style.borderTop = "0px solid black";
        minimapMaskPanel.style.borderBottom = "0px solid black";

        minimapMaskPanel.hittest = false;

    };

};

// ------------------------------------------------------------- //
// ----------------------- Minimap Regions --------------------- //
// ------------------------------------------------------------- //

function AttemptToRemoveRegion(key){
    var regionPanel = minimapRegions[key]["panel"];
    regionPanel.DeleteAsync(0);
    delete minimapRegions[key];
}

function UpdateRegion(key){

    var min_x = minimapRegions[key]["min_x"];
    var max_x = minimapRegions[key]["max_x"];
    var min_y = minimapRegions[key]["min_y"];
    var max_y = minimapRegions[key]["max_y"];
    var category = minimapRegions[key]["category"];
    var additionalStyle = minimapRegions[key]["style"];

    var regionPanel = CreateOrReferencePanel("Panel", dummyPanel, key);

    regionPanel.AddClass(additionalStyle);
    //regionPanel.hittest = false;

    // align is bottomleft
    var bot_corner = WorldToMinimapXY(min_x, min_y);
    var top_corner = WorldToMinimapXY(max_x, max_y);
    var panelWidth = top_corner[0] - bot_corner[0];
    var panelHeight = top_corner[1] - bot_corner[1];

    /*
    $.Msg("Minimap Panoramanager | Drawing a region marker with width: "+
                panelWidth+
                ", height: "+panelHeight+
                ", top_corner: "+top_corner[0]+", "+top_corner[1]+
                ", bot_corner: "+bot_corner[0]+", "+bot_corner[1]
                );
    */

    regionPanel.style.width = ""+panelWidth+"px";
    regionPanel.style.height = ""+panelHeight+"px";
    regionPanel.style.marginLeft = ""+bot_corner[0]+"px";
    regionPanel.style.marginBottom = ""+bot_corner[1]+"px";
    //regionPanel.style.transform = "translate3d("+bot_corner[0]+"px, -"+bot_corner[1]+"px, 0px)"

    // Tooltips and activation
    regionPanel.SetPanelEvent("onmouseover", function(){
        if ( !GameUI.IsMouseDown(1) &&
             !GameUI.IsShiftDown() &&
             !GameUI.IsAltDown() &&
             !GameUI.IsControlDown()
            ) { $.DispatchEvent("DOTAShowTextTooltip",
                     dummyPanel,
                     $.Localize(TOOLTIP_PREFIX+"_tooltip_minimap_region_"+category)
              )};
    });

    regionPanel.SetPanelEvent("onmouseout", function(){
        $.DispatchEvent("DOTAHideTextTooltip");
    });

}

// ------------------------------------------------------------- //
// ----------------------- Minimap Icons ----------------------- //
// ------------------------------------------------------------- //

// Update local state to match nettable
function OnEntityTableChange(table_name, key, data){
    //$.Msg("Making a minimap table update: "+key+", "+data)

    if (data == null) { // or some other falsy thing?
        AttemptToRemoveIcon(key)
    } else {

        if (!minimapIcons[key]) { // We need to create a new panel!
            //$.Msg("Making a new minimap icon!");
            minimapIcons[key] = {};
            var iconPanel = CreateOrReferencePanel("Panel", dummyPanel, key);
            iconPanel.AddClass("MinimapIcon");
            //iconPanel.hittest = false;
            iconPanel.SetParent(minimapOverlayPanel);
            minimapIcons[key]["panel"] = iconPanel;
        };

        minimapIcons[key]["icon"] = data["minimapIcon"];
        minimapIcons[key]["xposition"] = data["xposition"];
        minimapIcons[key]["yposition"] = data["yposition"];

        // Actually position the icon
        UpdateIcon(key)
        //$.Schedule(0.1, function() {UpdateIcon(key)} ) // This helps for *reasons*
        $.Schedule(1, function() {UpdateIcon(key)} )
        //$.Schedule(3, function() {UpdateIcon(key)} )
    }

};

function AttemptToRemoveIcon(key){
    $.Msg("Minimap panoramanager | AttemptToRemoveIcon called on:" + key)
    var iconPanel = minimapIcons[key]["panel"];
    iconPanel.DeleteAsync(0);
    delete minimapIcons[key];
};


// Called by the nettable updater
function UpdateIcon(entity){
    //$.Msg("Updating a minimap icon");

    var iconPanel = minimapIcons[entity]["panel"];
    var icon =  minimapIcons[entity]["icon"];
    var xposition = minimapIcons[entity]["xposition"];
    var yposition =  minimapIcons[entity]["yposition"];

    //$.Msg("Minimap Panoramananger | UpdateIcon | x position: "+xposition+", y position: "+yposition)

    if (ValidCoords(xposition,yposition) && iconVisibility[icon] && (icon != "no_icon")){

        //$.Msg("Placing the icon at "+xposition+", "+yposition);
        iconPanel.visible = true;

        PositionPanelAtMapCoordinates(iconPanel, xposition, yposition);

        iconPanel.style.backgroundImage = ICON_LOOKUP[icon];
        // Even though the class defines these, they don't render properly unless we force them
        iconPanel.style.backgroundSize = "70% 70%";
        iconPanel.style.backgroundRepeat = "no-repeat";
        iconPanel.style.backgroundPosition = "center";
        iconPanel.style.zIndex = "3";
    } else {
        iconPanel.visible = false;
    }

    // Tooltips and activation
    iconPanel.SetPanelEvent("onmouseover", function(){
        if ( !GameUI.IsMouseDown(1) &&
             !GameUI.IsShiftDown() &&
             !GameUI.IsAltDown() &&
             !GameUI.IsControlDown()
            ) { $.DispatchEvent("DOTAShowTextTooltip",
                     dummyPanel,
                     $.Localize(TOOLTIP_PREFIX+"_tooltip_minimap_icon_"+icon)
              )};
    });

    iconPanel.SetPanelEvent("onmouseout", function(){
        $.DispatchEvent("DOTAHideTextTooltip");
    });

};

function UpdateAllIcons(){
    UpdateAllMinimapIcons();
    UpdateAllHeroIcons();
}

function UpdateAllMinimapIcons(){
    for (var icon in minimapIcons){
        //$.Msg("Minimap Panoramanager | Updating icon: "+icon);
        UpdateIcon(icon);
    }
}

// Takes an array of icon types. Highlights all icons or regions on the minimap
// that match one of those types
function HighlightAll(iconTypes) {
    //$.Msg("Minimap Panoramanager | HighlightAll on : "+iconTypes)
    if (minimapIcons != {}){
        $.Each(minimapIcons, function(key, val){
            if (contains(iconTypes, key["icon"])) {
                HighlightMinimapIcon(key["panel"]);
            };
        })
    };

    if (minimapRegions != {}){
        $.Each(minimapRegions, function(key, val){
            if (contains(iconTypes, key["category"])) {
                var regionPanel = key["panel"];
                highlightedRegions.push(regionPanel);
                regionPanel.SetHasClass("Highlighted", true);
            };
        })
    };

};

// Given an iconpanel, create a new panel that highlights it
function HighlightMinimapIcon(iconPanel){
    var highlightIndicator = $.CreatePanel("Panel", dummyPanel, "");
    highlightedPanels.push(iconPanel);

    // Originally animations were awkward because of incorrect css inheritance
    // And so I used this style of animation
    // It could be swapped to an animation if needed, but this works fine
    if (iconPanel.visible){
        highlightIndicator.AddClass("ObjectiveMarker");
        highlightIndicator.AddClass("Swirl");
        $.Schedule(3, function(){
                try {
                highlightIndicator.RemoveClass("Swirl");
                highlightIndicator.AddClass("DoubleSwirl");
                } catch(e) {return true;} // No error spam if the highlight was deleted
                }
            );
        highlightIndicator.SetParent(iconPanel)

    } else {
        //$.Msg("Minimap Panoramanager | Tried to highlight an invalid icon");
    }
};

function RemoveAllHighlights(){
    for (var highlight of highlightedPanels) {
        highlight.RemoveAndDeleteChildren();
    }
    highlightedPanels = [];

    for (var region of highlightedRegions) {
        region.SetHasClass("Highlighted", false)
    }
    highlightedRegions = [];
};

// Toggle hiding for an array of ICON_LOOKUP elements
function HideIcons(iconTypes){
    //$.Msg("Minimap Panoramananger | HideIcons");
    for (var i = 0; i < iconTypes.length; i++){
        iconVisibility[iconTypes[i]] = !iconVisibility[iconTypes[i]];
    }
    UpdateAllIcons();
};


// TODO: Resize the minimap container itself
function ResizeMinimapContainer(){
    $.Msg("Running ResizeMinimap");

};

// ------------------------------------------------------------- //
// --------------------- "Hero" Minimap Icons ------------------ //
// ------------------------------------------------------------- //
/*
Hero minimap icons are managed panoramaside, rather than luaside.
This means that they can be updated more frequently without network
overhead, but they can't be used for certain specific features
(most notably, they are only visible if the local player has vision
of the target unit)
*/

function UpdateAllHeroIcons(){
    minimapContainerPanel.SetHasClass("AltPressed", GameUI.IsAltDown());

    for (var icon in heroIcons) {
        UpdateHeroIcon(icon);
    }
}

function RemoveHeroIcon(key){
    var intKey = Number(key);
    var panel = heroIcons[intKey]["panel"];
    panel.DeleteAsync(0);
    delete heroIcons[intKey];
}

function UpdateHeroIcon(entity){
    var nEntity = Number(entity) // forced coercion from lua string, just in case
    var panel = heroIcons[nEntity]["panel"];

    // Class for dead (replace moving icon with a dead marker and terminate)
    var isAlive = Entities.IsAlive(nEntity);
    panel.SetHasClass("DeadHeroIcon", !isAlive);
    
    var origin = Entities.GetAbsOrigin(nEntity);
    var forward = Entities.GetForward(nEntity);
    var angle = 225;
    if (forward != null) {
        angle += Math.atan2(forward[1], forward[0])*-57.2958;
    }

    var turnyPanel = panel.FindChildrenWithClassTraverse("TurnyCircle")[0];
    turnyPanel.style.transform = "rotateZ("+angle+"deg);"

    if (origin != null) {
        PositionPanelAtMapCoordinates(panel, origin[0], origin[1]);
    }

    /*
    Okay, hear me out here. DoTA-CSS doesn't allow for the !important
    flag, so the only way to determine priority is by JS
    (which hard-overwrites CSS and is irreversible), or by specificity.
    So, for these things, we're going to assign the "same" class multiple
    times, in order to increase the specificity of the selector, so that
    our desired class actually takes effect.
    */

    var local = Players.GetLocalPlayer() ;
    var owner = Entities.GetPlayerOwnerID( nEntity );
    var isInvis = IsUnitInvisible(nEntity);
    var isControllable = Entities.IsControllableByPlayer( nEntity, local );
    var isLocal = (local == owner);
    var isSelected = (Players.GetSelectedEntities( local )[0] == nEntity);

    // Class for unit stealth (inner circle should be transparent)
    panel.SetHasClass("HeroIconInvisA", isInvis);
    panel.SetHasClass("HeroIconInvisB", isInvis);
    panel.SetHasClass("HeroIconInvisC", isInvis);

    // Class for unit selected+owned (inner circle has black border)
    panel.SetHasClass("HeroIconSelectedA", isLocal && isSelected);
    panel.SetHasClass("HeroIconSelectedB", isLocal && isSelected);

    // Class for unit controllable+not owned (generic colour throughout)
    panel.SetHasClass("HeroIconSharedA", isControllable && !isLocal);
    panel.SetHasClass("HeroIconSharedB", isControllable && !isLocal);

}

// ------------------------------------------------------------- //
// ---------------------------- Layout ------------------------- //
// ------------------------------------------------------------- //

// React to any changes to the dota layout (flipped, widemap...)
function CheckLayout()
{
    var flipped = Game.IsHUDFlipped();

    minimapContainerSize = minimapContainerPanel.actuallayoutwidth/minimapContainerPanel.actualuiscale_x;

    var widemap = (minimapContainerSize > 260);

    var aspectRatio = Game.GetScreenWidth()/Game.GetScreenHeight();

    //$.Msg("Minimap Panoramanager | Checklayout flipped: "+flipped+
    //        " widemap: "+widemap+
    //        " aspectRatio: "+aspectRatio)

    if (currentlyFlipped != flipped || currentlyWideMap != widemap || aspectRatio != currentAspectRatio) {

        MoveObjectiveOverlay(flipped, widemap);
        UpdateMinimapBoundaries();
        UpdateAllIcons();
        $.Schedule(1, UpdateAllIcons);


    };

    currentlyFlipped = flipped;
    currentlyWideMap = widemap;
    currentAspectRatio = aspectRatio;

    $.Schedule(5, CheckLayout)
};

// Some of the commands here could be removed, using CSS inheritance instead
function MoveObjectiveOverlay(flipped, widemap){
    $.Msg("Minimap Panoramanager | Giving objective panel widemap: "+widemap+" flipped: "+flipped)
    objectiveRoot.SetHasClass("Flipped", flipped);
    objectiveRoot.SetHasClass("WideMap", widemap);

    primaryObjectivePanel.SetHasClass("WideMap", widemap);
    secondaryObjectivePanel.SetHasClass("WideMap", widemap);
    tertiaryObjectivePanel.SetHasClass("WideMap", widemap);
    objectivePanelToggle.SetHasClass("WideMap", widemap);

    for (var key in objectives){
        $.Msg("Minimap Panoramanager | Updating objective panel :"+key)
        var objIcon = objectives[key]["panel"].FindChildrenWithClassTraverse("Objective")[0];
        objIcon.SetHasClass("WideObjective", widemap);
    };
};


// ------------------------------------------------------------- //
// ------------------------ Initialization --------------------- //
// ------------------------------------------------------------- //

// Some of the HUD elements might not exist when the game starts
// In order to fix this, we will periodically check to see
// If we're ready for the minimap to load
function InitializeIfVisible(desiredPanel){
    if (desiredPanel.visible) {
        $.Msg("Minimap Panoramanager | Initializing Minimap");
        HookIntoDotaHud();
        CreateMinimapMask();
        UpdateMinimapBoundaries();
        UpdateAllIcons();

        $.Schedule(2, CheckLayout);

        // A fix for a rare condition where the minimap does not load initially.
        // Could need additional fixes or be taken out.
        if (minimapPanel.actuallayoutwidth == 0) {
            $.Schedule(1, function() {
                $.Msg("Minimap Panoramanager | Minimap had a false start");
                InitializeIfVisible(desiredPanel)
            })
        }

    } else {
        $.Schedule(1, function(){
            //$.Msg("Minimap Panoramanager | Minimap wasn't ready to load");
            InitializeIfVisible(desiredPanel)})
    }

};

// The minimap mask should have the same size as the (intended) minimap
// It sits on top of the minimap, adding a border over the parts
// that we don't want players to see
function CreateMinimapMask()
{
    minimapMaskPanel = CreateOrReferencePanel("Panel", minimapContainerPanel, "minimapMask");
    minimapMaskPanel.style.width = "100%";
    minimapMaskPanel.style.height = "100%";
    minimapMaskPanel.style.align = "middle middle";
    minimapMaskPanel.style.zIndex = "3";
};

// Assign variables for the necessary dota HUD elements
function HookIntoDotaHud() {

    $.Msg("Minimap Panoramanager | Hooking into HUD!")

    var hud = $.GetContextPanel().GetParent();
    for(var i = 0; i < 100; i++) {
        // $.Msg("DEBUG: id = " + hud.id);
        if(hud.id != "Hud") {
            hud = hud.GetParent();
        } else {
            break;
        }
    }

    // Covers the full span
    minimapOuterPanel = hud.FindChildTraverse("minimap_container");
    minimapOuterPanel.style.marginBottom = "3px";
    minimapOuterPanel.style.marginLeft = "3px";
    minimapOuterPanel.style.height = "480px";
    // Directly contains the minimap
    minimapContainerPanel = hud.FindChildTraverse("minimap_block");
    minimapContainerPanel.style.zIndex = "1";

    minimapOverlayPanel = CreateOrReferencePanel("Panel", minimapContainerPanel, "minimapOverlay")
    minimapOverlayPanel.style.width = "100%";
    minimapOverlayPanel.style.height = "100%";
    minimapOverlayPanel.style.zIndex = "2";
    minimapOverlayPanel.hittest = false;
    minimapOverlayPanel.style.overflow = "clip";

    // The minimap itself
    minimapPanel = hud.FindChildTraverse("minimap");
    minimapPanel.style.verticalAlign = "top";
    minimapPanel.style.horizontalAlign = "left";
    minimapPanel.style.zIndex = "1";

    //objectiveRoot = $("#ObjectiveRoot");
    if (objectiveRoot == null) { $.Msg("Minimap Panoramanager | Couldn't find objectiveroot. Panic!")}

    objectiveRoot.SetHasClass("Hidden", false);

    //------ Hook method (correct method for actual implementation) ----------//
    objectiveRoot.SetParent(minimapOuterPanel);
    objectiveRoot = minimapOuterPanel.FindChildTraverse("ObjectiveRoot");
    //------------- Hookless (overlaps with minimap, helps with testing) -----//
    //objectiveRoot = $("#ObjectiveRoot");


    objectivePanelToggle = objectiveRoot.FindChildTraverse("SizeTogglePanel");
    objectiveBackground = objectiveRoot.FindChildTraverse("ObjectiveBackground");
    objectiveMainPanel = objectiveRoot.FindChildTraverse("ObjectivePanel");

    primaryObjectivePanel = objectiveMainPanel.FindChildTraverse("PrimaryObjectivesBar");
    secondaryObjectivePanel = objectiveMainPanel.FindChildTraverse("SecondaryObjectivesBar");
    tertiaryObjectivePanel = objectiveMainPanel.FindChildTraverse("LandmarksBar");

};

// At game start, all icons are visible
function InitializeHiddenIcons(){
    for (var key in ICON_LOOKUP) {
        iconVisibility[key] = true;
    };
};


// ------------------------------------------------------------- //
// ----------------------------- IIVE -------------------------- //
// ------------------------------------------------------------- //

function StartHeroIconUpdateCycle(){
    UpdateAllHeroIcons();

    $.Schedule(HERO_ICON_UPDATE_INTERVAL, StartHeroIconUpdateCycle );
}

// Runs quite rarely, only really used for re-synchronisation purposes
function StartRegularIconUpdateCycle(){
    UpdateAllMinimapIcons()
    $.Schedule(REGULAR_ICON_UPDATE_INTERVAL, UpdateAllMinimapIcons);
}

function StartCameraLockCycle() {
    var pos = GameUI.GetCameraLookAtPosition();
    var x = pos[0];
    var y = pos[1];

    var new_x = x;
    var new_y = y;
    if (x<map_min_x) { new_x = map_min_x }
    if (x>map_max_x) { new_x = map_max_x }
    if (y<map_min_y) { new_y = map_min_y }
    if (y>map_max_y) { new_y = map_max_y }

    if (x!==new_x || y!== new_y) {
        GameUI.SetCameraTargetPosition([new_x, new_y, pos[2]] ,0.01);
    }

    $.Schedule(0.01, StartCameraLockCycle);
}

// Initialization functions
(function () {
    $.Msg("Minimap Panoramanager | Loading custom minimap ");

    var hud = $.GetContextPanel().GetParent();
    for(var i = 0; i < 100; i++) {
        if(hud.id != "Hud") {
            hud = hud.GetParent();
        } else {
            break;
        }
    };
    var desiredPanel = hud.FindChildTraverse("minimap");
    InitializeIfVisible(desiredPanel);

    $.Schedule(3, StartHeroIconUpdateCycle );
    $.Schedule(3, StartRegularIconUpdateCycle);

    CustomNetTables.SubscribeNetTableListener("minimap_settings", OnSettingTableChange)
    CustomNetTables.SubscribeNetTableListener("minimap_objects", OnEntityTableChange);
    CustomNetTables.SubscribeNetTableListener("minimap_regions", OnRegionTableChange);
    CustomNetTables.SubscribeNetTableListener("minimap_heroes", OnHeroTableChange);

    if ($("#ObjectiveRoot")) {
        $("#ObjectiveRoot").visible = OBJECTIVE_MANAGER_ACTIVE;
        if (OBJECTIVE_MANAGER_ACTIVE) {
            CustomNetTables.SubscribeNetTableListener("objectives", OnObjectiveTableChange);
        }
    }

    InitializeHiddenIcons();

    SynchroniseNetTables();

    $.Schedule(3, StartCameraLockCycle);

    minimapContainerSize = minimapContainerPanel.actuallayoutwidth/minimapContainerPanel.actualuiscale_x;

})();


